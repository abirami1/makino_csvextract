﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;

namespace makino_csvextract
{
    class Program
    {
        static string cur_date = DateTime.Now.ToString("yyyy-MM-dd");
        private static readonly HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            string connstring = "Server = localhost; Database = factory; Uid = root; Pwd = ;SslMode=none";
            MySqlConnection mcon = new MySqlConnection(connstring);
            mcon.Open();

            var files = Directory.GetFiles(@"F:\xampp\htdocs\alfadockpro\factory_layout\15\RPA", "*.csv");
            //var files = Directory.GetFiles(@"C:\Test", "*.csv");

            foreach (String f in files)
            {
                string fname = System.IO.Path.GetFileName(f);
                if (fname.Contains("Blanking.csv") || fname.Contains("Bending.csv"))
                {
                    extract_timedata(f, mcon);
                    //extract_timedata(f);
                }
                else if (fname.Contains("SheetSummary.csv"))
                {
                    extract_blankdata(f, mcon);
                    //extract_blankdata(f);
                }
                else if (fname.Contains("PartDetails.csv"))
                {
                    extract_benddata(f, mcon);
                    //extract_blankdata(f);
                }
            }

        }

        public static void extract_blankdata(string path, MySqlConnection mcon)
        //public static void extract_blankdata(string path)
        {
            try
            {
                bool isheader = true;

                using (StreamReader csv = new StreamReader(File.OpenRead(path)))
                {

                    Program pg = new Program();
                    while (!csv.EndOfStream)
                    {
                        var line = csv.ReadLine();
                        var values = line.Split(',');

                        if (isheader)
                        {
                            isheader = false;
                        }
                        else
                        {
                            if (values.Length > 10)
                            {


                                string pgno = values[2].Replace("\"", "");
                                string macname = values[1].Replace("\"", "");
                                string ddate = values[5].Replace("\"", "");
                                ddate = ddate.Replace("/", "-");
                                string stdate = (values[3] + " " + values[4]).Replace("\"", "");
                                string enddate = (values[5] + " " + values[6]).Replace("\"", "");
                                string actqty = (values[7]).Replace("\"", "");
                                string defqty = (values[8]).Replace("\"", "");
                                string actmactime = (values[9]).Replace("\"", "");

                                TimeSpan protime = TimeSpan.Parse(actmactime);
                                double ProcTimeSecs = protime.TotalSeconds;

                                string actmacsheet = (values[10]).Replace("\"", "");
                                string punchtime = (values[11]).Replace("\"", "");
                                string lasertime = (values[12]).Replace("\"", "");
                                string alarmtime = (values[13]).Replace("\"", "");
                                TimeSpan AlarmTime = TimeSpan.Parse(alarmtime);
                                double AlarmTimeSecs = AlarmTime.TotalSeconds;

                                string setuptime = (values[14]).Replace("\"", "");
                                string moldcount = (values[15]).Replace("\"", "");
                                string matname = (values[16]).Replace("\"", "");
                                string matcode = (values[17]).Replace("\"", "");
                                string thickness = (values[18]).Replace("\"", "");
                                string dimx = (values[19]).Replace("\"", "");
                                string dimy = (values[20]).Replace("\"", "");
                                string pgcomment = (values[21]).Replace("\"", "");

                                string CmdText = "INSERT IGNORE INTO makino_blankcsvextract VALUES(@idcsv,@sheetid,@machname,@progno,@progcomment,@materialcode,@materialname,@matx,@maty,@machiningtime,@starttime,@endtime,@actqty,@actprocesstime,@alarmtime,@setuptime,@punchtime,@lasertime,@moldcount,@thickness,@defect,@status,@prosecs,@alarmsecs)";
                                MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                                cmd.Parameters.AddWithValue("@idcsv", ddate + values[4].Replace("\"", ""));
                                cmd.Parameters.AddWithValue("@sheetid", "");
                                cmd.Parameters.AddWithValue("@machname", macname);
                                cmd.Parameters.AddWithValue("@progno", pgno);
                                cmd.Parameters.AddWithValue("@progcomment", Encoding.UTF8.GetBytes(pgcomment));
                                cmd.Parameters.AddWithValue("@materialcode", matcode);
                                cmd.Parameters.AddWithValue("@materialname", matname);
                                cmd.Parameters.AddWithValue("@matx", dimx);
                                cmd.Parameters.AddWithValue("@maty", dimy);
                                cmd.Parameters.AddWithValue("@machiningtime", actmactime);
                                cmd.Parameters.AddWithValue("@starttime", stdate);
                                cmd.Parameters.AddWithValue("@endtime", enddate);
                                cmd.Parameters.AddWithValue("@actqty", actqty);
                                cmd.Parameters.AddWithValue("@actprocesstime", actmacsheet);
                                cmd.Parameters.AddWithValue("@alarmtime", alarmtime);
                                cmd.Parameters.AddWithValue("@setuptime", setuptime);
                                cmd.Parameters.AddWithValue("@punchtime", punchtime);
                                cmd.Parameters.AddWithValue("@lasertime", lasertime);
                                cmd.Parameters.AddWithValue("@moldcount", moldcount);
                                cmd.Parameters.AddWithValue("@thickness", thickness);
                                cmd.Parameters.AddWithValue("@defect", defqty);
                                cmd.Parameters.AddWithValue("@status", "0");
                                cmd.Parameters.AddWithValue("@prosecs", ProcTimeSecs);
                                cmd.Parameters.AddWithValue("@alarmsecs", AlarmTimeSecs);

                                int result = cmd.ExecuteNonQuery();
                                int def_qty = Convert.ToInt16(defqty);
                                if (result == 1 && def_qty == 0)
                                {
                                   // pg.update_GPN(pgno, "10546", actqty, "3110", false, "-1", stdate.Replace("/", "-"), enddate.Replace("/", "-"), cur_date);


                                }
                                else if (result != 1)
                                {
                                    string query = string.Format(@"UPDATE makino_blankcsvextract SET machiningtime='{1}' , actprocesstime='{2}', prosecs={3}, alarmsecs={4} where idcsv='{0}' AND prosecs!={3}", ddate + values[4].Replace("\"", "") + pgno, actmactime, actmacsheet, ProcTimeSecs, AlarmTimeSecs);
                                    using (MySqlCommand command = new MySqlCommand(query, mcon))
                                    {
                                        int row = command.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }


                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("Error:" + e.ToString(), w);
                }
                Console.Write(Encoding.UTF8.GetBytes(e.ToString()));
            }
        }
        async void update_GPN(String partno, string pid, string qt, string userid, bool str_datefilter, string orderno, string start_date, string end_date, string due_date)
        {
            var str_arg = "{\"partNo\":\"" + partno + "\",\"processid\":\"" + pid + "\",\"status\":\"2\",\"quantity\":\"" + qt + "\",\"userid\":\"" + userid + "\",\"isSearchApplyDateFilters\":\"false\",\"setGivenTime\":\"true\",\"orderNo\":\"" + orderno + "\",\"startdate\":\"" + start_date + "\",\"enddate\":\"" + end_date + "\",\"dueDate\":\"" + due_date + "\"}";
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log(str_arg, w);
            }
            var values = new Dictionary<string, string>
{
    { "plugin", "SchedulerAPI" },
    { "controller", "SchedulerSubmitProcessController" },
    { "action", "SiProcessForLoadBalance" },
    { "args", str_arg }
};

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://13.115.237.181/api/plugin", content);

            //var responseString = 
            await response.Content.ReadAsStringAsync();

        }
        public static void extract_benddata(string path, MySqlConnection mcon)
        //public static void extract_blankdata(string path)
        {
            try
            {
                bool isheader = true;

                using (StreamReader csv = new StreamReader(File.OpenRead(path)))
                {

                    Program pg = new Program();
                    while (!csv.EndOfStream)
                    {
                        var line = csv.ReadLine();
                        var values = line.Split(',');

                        if (isheader)
                        {
                            isheader = false;
                        }
                        else
                        {
                            if (values.Length > 10)
                            {
                                string partno = values[3].Replace("\"", "");
                                string macname = values[1].Replace("\"", "");
                                string schdname = values[2].Replace("\"", "");
                                string ddate = values[21].Replace("\"", "");
                                ddate = ddate.Replace("/", "-");
                                string stdate = (values[19] + " " + values[20]).Replace("\"", "");
                                DateTime Procstarttime = DateTime.Parse(stdate);
                                string enddate = (values[21] + " " + values[22]).Replace("\"", "");
                                DateTime Procendtime = DateTime.Parse(enddate);

                                TimeSpan ProcTime = Procendtime - Procstarttime;
                                double ProcTimeSecs = ProcTime.TotalSeconds;
                                string actqty = (values[16]).Replace("\"", "");
                                string defqty = (values[25]).Replace("\"", "");
                                string actmactime = (values[38]).Replace("\"", "");
                                TimeSpan protime = TimeSpan.Parse(actmactime);
                                double actProcTimeSecs = protime.TotalSeconds;

                                string actmacsheet = "";
                                string punchtime = (values[35]).Replace("\"", "");
                                string lasertime = (values[37]).Replace("\"", "");
                                string alarmtime = (values[32]).Replace("\"", "");
                                TimeSpan AlarmTime = TimeSpan.Parse(alarmtime);
                                double AlarmTimeSecs = AlarmTime.TotalSeconds;

                                string setuptime = (values[36]).Replace("\"", "");
                                string moldcount = (values[34]).Replace("\"", "");
                                string matname = (values[10]).Replace("\"", "");
                                string matcode = (values[11]).Replace("\"", "");
                                string thickness = (values[9]).Replace("\"", "");
                                string dimx = (values[7]).Replace("\"", "");
                                string dimy = (values[8]).Replace("\"", "");
                                string pgcomment = (values[4]).Replace("\"", "");

                                string CmdText = "INSERT IGNORE INTO makino_bendcsvextract VALUES(@idcsv,@schedulename,@machname,@partno,@partcomment,@materialcode,@materialname,@matx,@maty,@machiningtime,@starttime,@endtime,@actqty,@actprocesstime,@alarmtime,@setuptime,@punchtime,@lasertime,@moldcount,@thickness,@defect,@status,@prosecs,@alarmsecs,@actprosecs)";
                                MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                                cmd.Parameters.AddWithValue("@idcsv", ddate + values[20].Replace("\"", ""));
                                cmd.Parameters.AddWithValue("@schedulename", schdname);
                                cmd.Parameters.AddWithValue("@machname", macname);
                                cmd.Parameters.AddWithValue("@partno", partno);
                                cmd.Parameters.AddWithValue("@partcomment", Encoding.UTF8.GetBytes(pgcomment));
                                cmd.Parameters.AddWithValue("@materialcode", matcode);
                                cmd.Parameters.AddWithValue("@materialname", matname);
                                cmd.Parameters.AddWithValue("@matx", dimx);
                                cmd.Parameters.AddWithValue("@maty", dimy);
                                cmd.Parameters.AddWithValue("@machiningtime", actmactime);
                                cmd.Parameters.AddWithValue("@starttime", stdate);
                                cmd.Parameters.AddWithValue("@endtime", enddate);
                                cmd.Parameters.AddWithValue("@actqty", actqty);
                                cmd.Parameters.AddWithValue("@actprocesstime", actmacsheet);
                                cmd.Parameters.AddWithValue("@alarmtime", alarmtime);
                                cmd.Parameters.AddWithValue("@setuptime", setuptime);
                                cmd.Parameters.AddWithValue("@punchtime", punchtime);
                                cmd.Parameters.AddWithValue("@lasertime", lasertime);
                                cmd.Parameters.AddWithValue("@moldcount", moldcount);
                                cmd.Parameters.AddWithValue("@thickness", thickness);
                                cmd.Parameters.AddWithValue("@defect", defqty);
                                cmd.Parameters.AddWithValue("@status", "0");
                                cmd.Parameters.AddWithValue("@prosecs", ProcTimeSecs);
                                cmd.Parameters.AddWithValue("@alarmsecs", AlarmTimeSecs);
                                cmd.Parameters.AddWithValue("@actprosecs", actProcTimeSecs);

                                int result = cmd.ExecuteNonQuery();
                                int def_qty = Convert.ToInt16(defqty);
                                if (result == 1 && def_qty == 0)
                                {

                                  //  pg.update_GPN(partno, "10548", actqty, "3110", false, "-1", stdate.Replace("/", "-"), enddate.Replace("/", "-"), cur_date);

                                }
                            }
                        }


                    }
                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("Error:" + e.ToString(), w);
                }
                Console.Write(Encoding.UTF8.GetBytes(e.ToString()));
            }
        }
        public static void extract_timedata(string path, MySqlConnection mcon)
        {
            try
            {
                bool isheader = true;

                using (StreamReader csv = new StreamReader(File.OpenRead(path)))
                {

                    while (!csv.EndOfStream)
                    {
                        var line = csv.ReadLine();
                        var values = line.Split(',');

                        if (isheader)
                        {
                            isheader = false;
                        }
                        else
                        {
                            if (values.Length > 5)
                            {
                                string date = values[0].Replace("\"", "");
                                string date1 = date.Replace("/", "-");
                                double poweroffsecs = 0;
                                if (!values[5].Equals("00:00:00"))
                                {
                                    poweroffsecs = Math.Round(Convert.ToDouble((TimeSpan.Parse(values[5].Replace("\"", "")).TotalSeconds) / 3600), 2);

                                }
                                double standbysecs = Convert.ToDouble(values[8].Replace("\"", ""));
                                double setupsecs = Convert.ToDouble(values[7].Replace("\"", ""));
                                double opsecs = Convert.ToDouble(values[6].Replace("\"", ""));
                                double alarmsecs = Convert.ToDouble(values[9].Replace("\"", ""));
                                string slots = "0";
                                if (values.Length > 11)
                                {
                                    slots = values[11].Replace("\"", "");
                                }
                                string CmdText = "INSERT IGNORE INTO makino_csvtime VALUES(@idcsv,@machname,@date,@poweroffsecs,@standbysecs,@setupsecs,@opsecs,@alarmsecs,@slots)";
                                MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                                cmd.Parameters.AddWithValue("@idcsv", date1 + "-" + values[4].Replace("\"", ""));
                                cmd.Parameters.AddWithValue("@machname", values[4].Replace("\"", ""));
                                cmd.Parameters.AddWithValue("@date", date1);
                                cmd.Parameters.AddWithValue("@poweroffsecs", poweroffsecs);
                                cmd.Parameters.AddWithValue("@standbysecs", standbysecs);
                                cmd.Parameters.AddWithValue("@setupsecs", setupsecs);
                                cmd.Parameters.AddWithValue("@opsecs", opsecs);
                                cmd.Parameters.AddWithValue("@alarmsecs", alarmsecs);
                                cmd.Parameters.AddWithValue("@slots", slots);
                                int result = cmd.ExecuteNonQuery();
                                if (result != 1)
                                {
                                    string query = string.Format(@"UPDATE makino_csvtime SET poweroffsecs={1} , standbysecs={2}, setupsecs={3}, opsecs={4}, alarmsecs={5}, slots={6} where  idcsv='{0}'", date1 + "-" + values[4].Replace("\"", ""), poweroffsecs, standbysecs, setupsecs, opsecs, alarmsecs, slots);
                                    using (MySqlCommand command = new MySqlCommand(query, mcon))
                                    {
                                        int row = command.ExecuteNonQuery();
                                    }
                                }

                            }
                        }
                    }


                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("Error:" + e.ToString(), w);
                }
                Console.Write(Encoding.UTF8.GetBytes(e.ToString()));
            }
        }


        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }

        public static void DumpLog(StreamReader r)
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }
    }

}
